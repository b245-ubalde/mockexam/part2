let collection = [];

// Write the queue functions below.

function print() {
    // It will show the array
    
    if(collection.length > 0){
        for (let i = 0; i < collection.length; i++){
            return collection[i];
        }
    }
    else{
        return collection;
    }
}



function enqueue(element) {
    //In this funtion you are going to make an algo that will add an element to the array
    // Mimic the function of push method

    collection[collection.length] = element;
    return collection;

}



function dequeue() {
    // In here you are going to remove the last element in the array

    for (let i = 1; i < collection.length; i++) {

        collection[i - 1] = collection[i];
        collection.length--;
    }

    return collection;

}

function front() {
    // In here, you are going to remove the first element

    // console.log(collection);
    return collection[0];
}



// starting from here, di na pwede gumamit ng .length property
function size() {
     // Number of elements  

   let  sizeCount = 0;
    for(let i = 0; collection[i] != null; i++){
        sizeCount = sizeCount + 1;
    }

    return sizeCount;
}

function isEmpty() {
    //it will check whether the function is empty or not

    if (collection === undefined || size() ===0){
            return  true;
        }
         return   false;
}





module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};